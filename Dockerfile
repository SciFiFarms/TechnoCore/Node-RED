FROM nodered/node-red-docker

# For credentials that look like paths, set file contents as credential.
COPY 10-mqtt.js /usr/src/node-red/node_modules/node-red/nodes/core/io/10-mqtt.js
COPY data/ /data
VOLUME /data